﻿using MySimpleAssignment.Domain.Dto;
using MySimpleAssignment.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySimpleAssignment.Infrastructure.Repositories.Interfaces
{
    public interface IProductRepository
    {
        Task<IEnumerable<ProductsWithCategoryDto>> GetAllProductsWithCategory();
        Task<IEnumerable<Product>> CreateAProductWithCategory(ProductsWithCategoryDto productsWithCategoryDto);
        Task<IEnumerable<Product>> UpdateProductWithCategory(ProductsWithCategoryDto productsWithCategoryDto);
        Task<bool> IsProductExisted(string name);
        Task<bool> DeleteAProduct(string productId, string categoryId);
        Task<IEnumerable<ProductsWithCategoryDto>> GetProduct(string id);
    }
}
