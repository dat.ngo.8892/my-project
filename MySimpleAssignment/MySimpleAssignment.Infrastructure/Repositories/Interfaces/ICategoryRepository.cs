﻿using MySimpleAssignment.Domain.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySimpleAssignment.Infrastructure.Repositories.Interfaces
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<CategoryDto>> CreateCategory(CategoryDto categoryDto);
    }
}
