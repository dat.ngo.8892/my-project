﻿using MySimpleAssignment.Domain.Dto;
using MySimpleAssignment.Domain.Entities;
using MySimpleAssignment.Domain.Relationships;
using MySimpleAssignment.Infrastructure.Repositories.Interfaces;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySimpleAssignment.Infrastructure.Repositories.Implementations
{
    public class ProductRepository : IProductRepository
    {
        private readonly IBoltGraphClient _graphClient;

        public ProductRepository(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }

        public async Task<IEnumerable<ProductsWithCategoryDto>> GetAllProductsWithCategory()
        {
            var query = _graphClient.Cypher.Read
                .Match($"(p:{Product.LABEL_PRODUCT}) <-[r:{HasProduct.LABEL_HAS_PRODUCT}]- (c:{Category.LABEL_CATEGORY})")
                .With($"p{{.*, CategoryId: c.{nameof(Category.Id)}, Category: c.{nameof(Category.Name)}}} as p")
                .Return<ProductsWithCategoryDto>("p");
            var result = await query.ResultsAsync;

            if(result == null)
            {
                new List<ProductsWithCategoryDto>();
            }

            return result;

        }

        public async Task<IEnumerable<Product>> CreateAProductWithCategory(ProductsWithCategoryDto productsWithCategory)
        {
            var query = _graphClient.Cypher.Write
                .Match($"(c:{Category.LABEL_CATEGORY})")
                .Where($"(c.Id = '{productsWithCategory.CategoryId}')")
                .Merge($"(p:{Product.LABEL_PRODUCT} " +
                $"{{{nameof(Product.Name)}: '{productsWithCategory.Name}'" +
                $"}}) <-[:{HadProduct.LABEL_HAD_PRODUCT}]- (c) ")
                .OnCreate()
                .Set($"p.{nameof(Product.CreatedDate)} = '{productsWithCategory.CreatedDate}', " +
                $"p.{nameof(Product.Description)}= '{productsWithCategory.Description}', " +
                $"p.{nameof(Product.Version)}= {productsWithCategory.Version}, " +
                $"p.{nameof(Product.Id)}= '{productsWithCategory.Id}'")
                .OnMatch()
                .Set($"p.{nameof(Product.UpdatedDate)} = '{productsWithCategory.CreatedDate}', " +
                $"p.{nameof(Product.Description)}= '{productsWithCategory.Description}', " +
                $"p.{nameof(Product.Version)}= {productsWithCategory.Version++} ")
                .Merge($"(c) -[r:{HasProduct.LABEL_HAS_PRODUCT}]-> (p)")
                .Return<Product>("p");

            var result = await query.ResultsAsync;
            return result;
        }

        public async Task<IEnumerable<Product>> UpdateProductWithCategory(ProductsWithCategoryDto productsWithCategory)
        {
            var query = _graphClient.Cypher.Write
                .Match($"(cNew:{Category.LABEL_CATEGORY} {{Name:'{productsWithCategory.Category}'}})")
                .Match($"(p:{Product.LABEL_PRODUCT} {{Id:'{productsWithCategory.Id}'}}) <-[r:{HasProduct.LABEL_HAS_PRODUCT}]- (c)")
                .Merge($"((p) <-[:{HadProduct.LABEL_HAD_PRODUCT}]- (c))")
                .Delete("r")
                .Merge($"((p) <-[:{HasProduct.LABEL_HAS_PRODUCT}]- (cNew))")
                .Set($"p.{nameof(Product.Version)} = {productsWithCategory.Version}, " +
                $"p.{nameof(Product.Description)} = '{productsWithCategory.Description}', " +
                $"p.{nameof(productsWithCategory.UpdatedDate)} = '{productsWithCategory.UpdatedDate}' ")
                .Return<Product>("p");

            var result = await query.ResultsAsync;
            return result;
        }

        public async Task<bool> IsProductExisted(string name)
        {
            var query = _graphClient.Cypher.Read
                .Match($"(p:{Product.LABEL_PRODUCT}{{Name:$name}})")
                .WithParam("name", name)
                .Match($"(p) <-[:HAS_PRODUCT]- (c:{nameof(Category.LABEL_CATEGORY)})")
                .Return<ProductDto>("p");

            var result = await query.ResultsAsync;

            return  result.ToList().Count > 0 ? true : false;
        }

        public async Task<bool> DeleteAProduct(string productId, string categoryId)
        {
            var query = _graphClient.Cypher.Write
                .Match($"(p:{Product.LABEL_PRODUCT}{{Id:$productId}})")
                .WithParam("productId", productId)
                .Match($"(c:{Category.LABEL_CATEGORY}{{Id:$categoryId}})")
                .WithParam("categoryId", categoryId)
                .Match($"(p) <-[r:{HasProduct.LABEL_HAS_PRODUCT}]- (c)")
                .Merge($"(p) <-[rNew:{HadProduct.LABEL_HAD_PRODUCT}]- (c)")
                .Delete("r")
                .Return<HadProduct>("rNew");

            var result = await query.ResultsAsync;

            return result.ToList().Count > 0 ? false : true;
        }

        public async Task<IEnumerable<ProductsWithCategoryDto>> GetProduct(string id)
        {
            var query = _graphClient.Cypher.Read
                .Match($"(p:{Product.LABEL_PRODUCT} {{{nameof(Product.Id)}: $productId}})")
                .WithParam("productId", id)
                .Match($"(p) <-[r:{HasProduct.LABEL_HAS_PRODUCT}]- (c:{Category.LABEL_CATEGORY})")
                .With($"p{{.*, CategoryId: c.{nameof(Category.Id)}, Category: c.{nameof(Category.Name)}}} as p")
                .Return<ProductsWithCategoryDto>("*");

            var result = await query.ResultsAsync;
            return result;
        }

    }
}
