﻿using MySimpleAssignment.Domain.Dto;
using MySimpleAssignment.Domain.Entities;
using MySimpleAssignment.Infrastructure.Repositories.Interfaces;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MySimpleAssignment.Infrastructure.Repositories.Implementations
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly IBoltGraphClient _graphClient;

        public CategoryRepository(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }
        public async Task<IEnumerable<CategoryDto>> CreateCategory(CategoryDto categoryDto)
        {
            var query = _graphClient.Cypher.Write
                .Create($"(c:{Category.LABEL_CATEGORY} {{ {nameof(Category.Id)}: '{categoryDto.Id}', " +
                $"{nameof(Category.Name)}: '{categoryDto.Name}', " +
                $"{nameof(Category.Version)}: '{categoryDto.Version}'," +
                $"{nameof(Category.Version)}: '{categoryDto.Version}'," +
                $"{nameof(Category.CreatedDate)}: '{categoryDto.CreatedDate}'" +
                $"}})")
                .Return<CategoryDto>("c");

            var result = await query.ResultsAsync;
            return result;
        }
    }
}
