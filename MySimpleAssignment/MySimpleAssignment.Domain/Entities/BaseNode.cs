﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MySimpleAssignment.Domain.Entities
{
    public abstract class BaseNode
    {
        private readonly string _label;

        protected BaseNode(string label)
        {
            _label = !string.IsNullOrWhiteSpace(label) ? label : this.GetType().Name;
        }

        public virtual string Id { get; set; }

        [JsonIgnore]
        [JsonProperty(Order = 50)]
        public int Version { get; set; }

        [JsonIgnore]
        [JsonProperty(Order = 51)]
        public DateTime CreatedDate { get; set; }

        [JsonIgnore]
        [JsonProperty(Order = 52)]
        public DateTime? UpdatedDate { get; set; }

        [JsonIgnore]
        public string Label => _label;
    }
}
