﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MySimpleAssignment.Domain.Entities
{
    public class Product : BaseNode
    {
        public const string LABEL_PRODUCT = "Product";
        public Product() : base(LABEL_PRODUCT)
        {
        }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
