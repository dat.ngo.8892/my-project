﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MySimpleAssignment.Domain.Relationships
{
    public class HadProduct : BaseRelationship
    {
        public const string LABEL_HAD_PRODUCT = "HAD_PRODUCT";
        public HadProduct() : base(LABEL_HAD_PRODUCT)
        { }
    }
}
