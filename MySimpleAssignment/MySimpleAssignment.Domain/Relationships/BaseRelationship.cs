﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MySimpleAssignment.Domain.Relationships
{
    public abstract class BaseRelationship
    {
        private readonly string _label;

        public BaseRelationship(string label)
        {
            _label = !string.IsNullOrWhiteSpace(label) ? label : this.GetType().Name;
        }


        [JsonIgnore]
        public string Label => _label;
    }
}
