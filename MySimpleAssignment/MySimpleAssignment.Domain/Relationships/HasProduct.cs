﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MySimpleAssignment.Domain.Relationships
{
    public class HasProduct : BaseRelationship
    {
        public const string LABEL_HAS_PRODUCT = "HAS_PRODUCT";
        public HasProduct() : base(LABEL_HAS_PRODUCT)
        { }
    }
}
