﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MySimpleAssignment.Domain.Dto
{
    public class ProductDto
    {
        public string Id { get; set; }
        public int Version { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
