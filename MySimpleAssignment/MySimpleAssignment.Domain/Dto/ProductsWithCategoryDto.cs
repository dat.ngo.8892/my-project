﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MySimpleAssignment.Domain.Dto
{
    public class ProductsWithCategoryDto
    {
        [JsonProperty]
        public string Id { get; set; }
        [JsonProperty]
        public int Version { get; set; }
        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public string Description { get; set; }
        [JsonProperty]
        public DateTime CreatedDate { get; set; }
        [JsonProperty]
        public DateTime? UpdatedDate { get; set; }
        [JsonProperty]
        public string Category { get; set; }
        [JsonProperty]
        public string CategoryId { get; set; }
    }
}
