﻿using MySimpleAssignment.Domain.Dto;
using MySimpleAssignment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MySimpleAssignment.Services.Services.Interfaces
{
    public interface IProductService
    { 
        Task<IEnumerable<ProductsWithCategoryDto>> GetAllProductsWithCategory();
        Task<IEnumerable<Product>> CreateAProductWithCategory(ProductsWithCategoryDto productsWithCategoryDto);
        Task<bool> IsProductExisted(string name);
        Task<IEnumerable<Product>> UpdateAProductWithCategory(ProductsWithCategoryDto productsWithCategory);
        Task<bool> DeleteAProduct(string productId, string categoryId);
        Task<IEnumerable<ProductsWithCategoryDto>> GetProduct(string id);
    }
}
