﻿using MySimpleAssignment.Domain.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MySimpleAssignment.Services.Services.Interfaces
{
    public interface ICategoryServices
    {
        Task<IEnumerable<CategoryDto>> CreateCategory(CategoryDto categoryDto); 
    }
}
