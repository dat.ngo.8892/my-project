﻿using MySimpleAssignment.Domain.Dto;
using MySimpleAssignment.Domain.Entities;
using MySimpleAssignment.Infrastructure.Repositories.Interfaces;
using MySimpleAssignment.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MySimpleAssignment.Services.Services.Implementations
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<IEnumerable<Product>> CreateAProductWithCategory(ProductsWithCategoryDto productsWithCategoryDto)
        {
            productsWithCategoryDto.Id = Guid.NewGuid().ToString();
            productsWithCategoryDto.Version = 1;
            productsWithCategoryDto.CreatedDate = DateTime.Now;
            productsWithCategoryDto.UpdatedDate = null;
            var product = await _productRepository.CreateAProductWithCategory(productsWithCategoryDto);
            return product;
        }

        public async Task<bool> DeleteAProduct(string productId, string categoryId)
        {
            var result = await _productRepository.DeleteAProduct(productId, categoryId);
            return result;
        }

        public async Task<IEnumerable<ProductsWithCategoryDto>> GetAllProductsWithCategory()
        {
            var products = await _productRepository.GetAllProductsWithCategory();

            return products;
        }

        public async Task<bool> IsProductExisted(string name)
        {
            var result = await _productRepository.IsProductExisted(name);
            return result;
        }

        public async Task<IEnumerable<Product>> UpdateAProductWithCategory(ProductsWithCategoryDto productsWithCategory)
        {
            productsWithCategory.Version += 1;
            productsWithCategory.UpdatedDate = DateTime.Now;
            var products = await _productRepository.UpdateProductWithCategory(productsWithCategory);

            return products;
        }
        public async Task<IEnumerable<ProductsWithCategoryDto>> GetProduct(string id)
        {
            var result = await _productRepository.GetProduct(id);
            return result;
        }
    }
}
