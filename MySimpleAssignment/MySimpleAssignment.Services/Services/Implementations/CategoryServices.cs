﻿using MySimpleAssignment.Domain.Dto;
using MySimpleAssignment.Infrastructure.Repositories.Interfaces;
using MySimpleAssignment.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MySimpleAssignment.Services.Services.Implementations
{
    public class CategoryServices : ICategoryServices
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryServices(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<IEnumerable<CategoryDto>> CreateCategory(CategoryDto categoryDto)
        {
            categoryDto.Id = Guid.NewGuid().ToString();
            categoryDto.Version = 1;
            categoryDto.CreatedDate = DateTime.Now;
            var result = await _categoryRepository.CreateCategory(categoryDto);
            return  result;
        }
    }
}
