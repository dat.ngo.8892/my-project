﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySimpleAssignment.Domain.Dto;
using MySimpleAssignment.Services.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace MySimpleAssignment.API.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryServices _categoryServices;
        public CategoryController(ICategoryServices categoryServices)
        {
            _categoryServices = categoryServices;
        }


        /// <summary>
        /// create a category
        /// </summary>
        /// <param name="value"></param>
        [HttpPost]
        [Route("createcategory")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(IEnumerable<CategoryDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCategory(CategoryDto categoryDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest, $"Invalid due to bad request");
                }

                if (categoryDto.Name == null || categoryDto.Name == "")
                {
                    return StatusCode(409);
                }

                var result = await _categoryServices.CreateCategory(categoryDto);


                return result == null ? StatusCode(409) : StatusCode(201);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, $"Unable to update data due to: {ex.Message}");
            }
        }
    }
}
