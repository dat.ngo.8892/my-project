﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySimpleAssignment.Domain.Dto;
using MySimpleAssignment.Services.Services.Interfaces;

namespace MySimpleAssignment.API.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(IEnumerable<ProductDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Index()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest, $"Invalid due to bad request");
                }
                var result = await _productService.GetAllProductsWithCategory();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, $"Unable to get data due to: {ex.Message}");
            }
        }

        [HttpGet]
        [Route("getproduct")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(IEnumerable<ProductDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetProduct([FromQuery(Name = "productId")] string productId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest, $"Invalid due to bad request");
                }
                if (productId == "" || productId == null)
                {
                    return StatusCode(204);
                }

                var result = await _productService.GetProduct(productId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, $"Unable to get data due to: {ex.Message}");
            }
        }

        [HttpPost]
        [Route("createproduct")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(IEnumerable<ProductDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAProductWithCategory(ProductsWithCategoryDto productsWithCategory)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest, $"Invalid due to bad request");
                }

                if (productsWithCategory.Name == "" || productsWithCategory == null)
                {
                    return StatusCode(204);
                }

                bool isProductIdExisted = await _productService.IsProductExisted(productsWithCategory.Name);
                
                //if (isProductIdExisted)
                //{
                //    return StatusCode(409);
                //}

                var result = await _productService.CreateAProductWithCategory(productsWithCategory);

                return result == null ? StatusCode(500) : StatusCode(201);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, $"Unable to create data due to: {ex.Message}");
            }
        }

        [HttpPut]
        [Route("updateproduct")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(IEnumerable<ProductDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAProductWithCategory(ProductsWithCategoryDto productsWithCategory)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest, $"Invalid due to bad request");
                }

                if (productsWithCategory.Category == null || productsWithCategory.Category == "")
                {
                    return StatusCode(409);
                }

                var result = await _productService.UpdateAProductWithCategory(productsWithCategory);

                return result == null ? StatusCode(500) : StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, $"Unable to update data due to: {ex.Message}");
            }
        }

        [HttpDelete]
        [Route("deleteproduct")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(IEnumerable<ProductDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteProduct(string productId, string categoryId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest, $"Invalid due to bad request");
                }

                if (productId == null || productId == "" || categoryId == null || categoryId == "")
                {
                    return StatusCode(409);
                }

                var result = await _productService.DeleteAProduct(productId, categoryId);

                return result == true ? StatusCode(500) : StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, $"Unable to update data due to: {ex.Message}");
            }
        }


    }
}
