using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MySimpleAssignment.Infrastructure.Models;
using MySimpleAssignment.Infrastructure.Repositories.Implementations;
using MySimpleAssignment.Infrastructure.Repositories.Interfaces;
using MySimpleAssignment.Services.Services.Implementations;
using MySimpleAssignment.Services.Services.Interfaces;
using Neo4j.Driver.V1;
using Neo4jClient;

namespace MySimpleAssignment.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddControllers();
            RegisterGraphDriver(services);
            RegisterGraphClient(services);
            RegisterRepositories(services);
            RegisterServices(services);
            services.AddSwaggerGen();
        }

        private IServiceCollection RegisterServices(IServiceCollection services)
        {
            services.AddTransient(typeof(IProductService), typeof(ProductService));
            services.AddTransient(typeof(ICategoryServices), typeof(CategoryServices));
            return services;
        }

        private IServiceCollection RegisterRepositories(IServiceCollection services)
        {
            services.AddTransient(typeof(IProductRepository), typeof(ProductRepository));
            services.AddTransient(typeof(ICategoryRepository), typeof(CategoryRepository));
            return services;
        }

        private IServiceCollection RegisterGraphDriver(IServiceCollection services)
        {
            services.AddSingleton(typeof(IDriver), resolver =>
            {
                var neo4jSetting = Configuration.GetSection("Neo4jSetting").Get<Neo4jSetting>();
                var authToken = AuthTokens.Basic(neo4jSetting.UserName, neo4jSetting.Password);
                var config = new Config();
                config = Config.Builder.WithEncryptionLevel(EncryptionLevel.None).ToConfig();
                var driver = GraphDatabase.Driver(neo4jSetting.BoltUrl, authToken, config);
                return driver;
            });

            return services;
        }
        private IServiceCollection RegisterGraphClient(IServiceCollection services)
        {
            services.AddSingleton(typeof(IBoltGraphClient), resolver =>
            {
                var client = new BoltGraphClient(services.BuildServiceProvider().GetService<IDriver>());

                if (!client.IsConnected)
                {
                    client.Connect();
                }

                return client;
            });

            return services;
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            app.UseCors("AllowAll");

            app.UseRouting();

            app.UseAuthorization();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=[controller]}/{action=[action]}/{id?}");
            });
        }
    }
}
    
